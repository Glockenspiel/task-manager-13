package ru.t1.sukhorukova.tm.repository;

import ru.t1.sukhorukova.tm.api.repository.ICommandRepository;
import ru.t1.sukhorukova.tm.constant.ArgumentConst;
import ru.t1.sukhorukova.tm.constant.CommandConst;
import ru.t1.sukhorukova.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command CMD_HELP = new Command(CommandConst.CMD_HELP, ArgumentConst.CMD_HELP, "Show help.");
    public static final Command CMD_VERSION = new Command(CommandConst.CMD_VERSION, ArgumentConst.CMD_VERSION, "Show version.");
    public static final Command CMD_ABOUT = new Command(CommandConst.CMD_ABOUT, ArgumentConst.CMD_ABOUT, "Show about developer.");
    public static final Command CMD_INFO = new Command(CommandConst.CMD_INFO, ArgumentConst.CMD_INFO, "Show system information.");
    public static final Command CMD_ARGUMENT = new Command(CommandConst.CMD_ARGUMENT, ArgumentConst.CMD_ARGUMENT, "Show argument list.");
    public static final Command CMD_COMMAND = new Command(CommandConst.CMD_COMMAND, ArgumentConst.CMD_COMMAND, "Show command list.");

    public static final Command CMD_PROJECT_CREATE = new Command(CommandConst.CMD_PROJECT_CREATE, null, "Create new project.");
    public static final Command CMD_PROJECT_LIST = new Command(CommandConst.CMD_PROJECT_LIST, null, "Show project list.");
    public static final Command CMD_PROJECT_SHOW_BY_ID = new Command(CommandConst.CMD_PROJECT_SHOW_BY_ID, null, "Display project by id.");
    public static final Command CMD_PROJECT_SHOW_BY_INDEX = new Command(CommandConst.CMD_PROJECT_SHOW_BY_INDEX, null, "Display project by index.");
    public static final Command CMD_PROJECT_UPDATE_BY_ID = new Command(CommandConst.CMD_PROJECT_UPDATE_BY_ID, null, "Update project by id.");
    public static final Command CMD_PROJECT_UPDATE_BY_INDEX = new Command(CommandConst.CMD_PROJECT_UPDATE_BY_INDEX, null, "Update project by index.");
    public static final Command CMD_PROJECT_REMOVE_BY_ID = new Command(CommandConst.CMD_PROJECT_REMOVE_BY_ID, null, "Remove project by id.");
    public static final Command CMD_PROJECT_REMOVE_BY_INDEX = new Command(CommandConst.CMD_PROJECT_REMOVE_BY_INDEX, null, "Remove project by index.");
    public static final Command CMD_PROJECT_CLEAR = new Command(CommandConst.CMD_PROJECT_CLEAR, null, "Remove all projects.");
    public static final Command CMD_PROJECT_CHANGE_STATUS_BY_ID = new Command(CommandConst.CMD_PROJECT_CHANGE_STATUS_BY_ID, null, "Change project status by id.");
    public static final Command CMD_PROJECT_CHANGE_STATUS_BY_INDEX = new Command(CommandConst.CMD_PROJECT_CHANGE_STATUS_BY_INDEX, null, "Change project status by index.");
    public static final Command CMD_PROJECT_START_BY_ID = new Command(CommandConst.CMD_PROJECT_START_BY_ID, null, "Start project by id.");
    public static final Command CMD_PROJECT_START_BY_INDEX = new Command(CommandConst.CMD_PROJECT_START_BY_INDEX, null, "Start project by index.");
    public static final Command CMD_PROJECT_COMPLETE_BY_ID = new Command(CommandConst.CMD_PROJECT_COMPLETE_BY_ID, null, "Complete project by id.");
    public static final Command CMD_PROJECT_COMPLETE_BY_INDEX = new Command(CommandConst.CMD_PROJECT_COMPLETE_BY_INDEX, null, "Complete project by index.");

    public static final Command CMD_TASK_CREATE = new Command(CommandConst.CMD_TASK_CREATE, null, "Create new task.");
    public static final Command CMD_TASK_LIST = new Command(CommandConst.CMD_TASK_LIST, null, "Show task list.");
    public static final Command CMD_TASK_SHOW_BY_ID = new Command(CommandConst.CMD_TASK_SHOW_BY_ID, null, "Display task by id.");
    public static final Command CMD_TASK_SHOW_BY_INDEX = new Command(CommandConst.CMD_TASK_SHOW_BY_INDEX, null, "Display task by index.");
    public static final Command CMD_TASK_UPDATE_BY_ID = new Command(CommandConst.CMD_TASK_UPDATE_BY_ID, null, "Update task by id.");
    public static final Command CMD_TASK_UPDATE_BY_INDEX = new Command(CommandConst.CMD_TASK_UPDATE_BY_INDEX, null, "Update task by index.");
    public static final Command CMD_TASK_REMOVE_BY_ID = new Command(CommandConst.CMD_TASK_REMOVE_BY_ID, null, "Remove task by id.");
    public static final Command CMD_TASK_REMOVE_BY_INDEX = new Command(CommandConst.CMD_TASK_REMOVE_BY_INDEX, null, "Remove task by index.");
    public static final Command CMD_TASK_CLEAR = new Command(CommandConst.CMD_TASK_CLEAR, null, "Remove all tasks.");
    public static final Command CMD_TASK_CHANGE_STATUS_BY_ID = new Command(CommandConst.CMD_TASK_CHANGE_STATUS_BY_ID, null, "Change task status by id.");
    public static final Command CMD_TASK_CHANGE_STATUS_BY_INDEX = new Command(CommandConst.CMD_TASK_CHANGE_STATUS_BY_INDEX, null, "Change task status by index.");
    public static final Command CMD_TASK_START_BY_ID = new Command(CommandConst.CMD_TASK_START_BY_ID, null, "Start task by id.");
    public static final Command CMD_TASK_START_BY_INDEX = new Command(CommandConst.CMD_TASK_START_BY_INDEX, null, "Start task by index.");
    public static final Command CMD_TASK_COMPLETE_BY_ID = new Command(CommandConst.CMD_TASK_COMPLETE_BY_ID, null, "Complete task by id.");
    public static final Command CMD_TASK_COMPLETE_BY_INDEX = new Command(CommandConst.CMD_TASK_COMPLETE_BY_INDEX, null, "Complete task by index.");

    public static final Command CMD_TASK_BIND_TO_PROJECT = new Command(CommandConst.CMD_TASK_BIND_TO_PROJECT, null, "Bind task to project.");
    public static final Command CMD_TASK_UNBIND_FROM_PROJECT = new Command(CommandConst.CMD_TASK_UNBIND_FROM_PROJECT, null, "Unbind task from project.");
    public static final Command CMD_TASK_SHOW_BY_PROJECT_ID = new Command(CommandConst.CMD_TASK_SHOW_BY_PROJECT_ID, null, "Show task list by project id.");

    public static final Command CMD_EXIT = new Command(CommandConst.CMD_EXIT, null, "Close application.");

    public static final Command[] TERMINAL_COMMANDS = {
            CMD_HELP, CMD_VERSION, CMD_ABOUT, CMD_INFO, CMD_ARGUMENT, CMD_COMMAND,

            CMD_PROJECT_CREATE,
            CMD_PROJECT_LIST, CMD_PROJECT_SHOW_BY_ID, CMD_PROJECT_SHOW_BY_INDEX,
            CMD_PROJECT_UPDATE_BY_ID, CMD_PROJECT_UPDATE_BY_INDEX,
            CMD_PROJECT_REMOVE_BY_ID, CMD_PROJECT_REMOVE_BY_INDEX, CMD_PROJECT_CLEAR,
            CMD_PROJECT_CHANGE_STATUS_BY_ID, CMD_PROJECT_CHANGE_STATUS_BY_INDEX,
            CMD_PROJECT_START_BY_ID, CMD_PROJECT_START_BY_INDEX,
            CMD_PROJECT_COMPLETE_BY_ID, CMD_PROJECT_COMPLETE_BY_INDEX,

            CMD_TASK_CREATE,
            CMD_TASK_LIST, CMD_TASK_SHOW_BY_ID, CMD_TASK_SHOW_BY_INDEX,
            CMD_TASK_UPDATE_BY_ID, CMD_TASK_UPDATE_BY_INDEX,
            CMD_TASK_REMOVE_BY_ID, CMD_TASK_REMOVE_BY_INDEX, CMD_TASK_CLEAR,
            CMD_TASK_CHANGE_STATUS_BY_ID, CMD_TASK_CHANGE_STATUS_BY_INDEX,
            CMD_TASK_START_BY_ID, CMD_TASK_START_BY_INDEX,
            CMD_TASK_COMPLETE_BY_ID, CMD_TASK_COMPLETE_BY_INDEX,

            CMD_TASK_BIND_TO_PROJECT, CMD_TASK_UNBIND_FROM_PROJECT,
            CMD_TASK_SHOW_BY_PROJECT_ID,

            CMD_EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
