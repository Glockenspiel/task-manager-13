package ru.t1.sukhorukova.tm.api.controller;

public interface ICommandController {

    void showInfo();

    void showArgumentError();

    void showCommandError();

    void showVersion();

    void showAbout();

    void showArguments();

    void showCommands();

    void showHelp();

}
