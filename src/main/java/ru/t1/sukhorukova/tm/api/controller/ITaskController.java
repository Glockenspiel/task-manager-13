package ru.t1.sukhorukova.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTasks();

    void showTasksByProjectId();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void clearTasks();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

}
