package ru.t1.sukhorukova.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void clearProjects();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

}
