package ru.t1.sukhorukova.tm.controller;

import ru.t1.sukhorukova.tm.api.controller.IProjectTaskController;
import ru.t1.sukhorukova.tm.api.service.IProjectTaskService;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("BIND TASK TO PROJECT");

        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();

        System.out.println("Enter task id:");
        final String taskId = TerminalUtil.nextLine();

        projectTaskService.bindTaskToProject(projectId, taskId);
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("UNBIND TASK FROM PROJECT");

        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();

        System.out.println("Enter task id:");
        final String taskId = TerminalUtil.nextLine();

        projectTaskService.unbindTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
    }

}
