package ru.t1.sukhorukova.tm.controller;

import ru.t1.sukhorukova.tm.api.controller.IProjectController;
import ru.t1.sukhorukova.tm.api.controller.IProjectTaskController;
import ru.t1.sukhorukova.tm.api.service.IProjectService;
import ru.t1.sukhorukova.tm.api.service.IProjectTaskService;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;
    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void createProject() {
        System.out.println("[PROJECT CREATE]");

        System.out.println("Enter project name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter project description:");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.create(name, description);
        if (project != null) System.out.println("[OK]");
        else System.out.println("[ERROR]");
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT BY ID]");

        System.out.println("Enter project id:");
        final String id = TerminalUtil.nextLine();

        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT BY INDEX]");

        System.out.println("Enter project index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");

        System.out.println("Enter project id:");
        final String id = TerminalUtil.nextLine();

        System.out.println("Enter project name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter project description:");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.updateById(id, name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");

    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");

        System.out.println("Enter project index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("Enter project name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter project description:");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.updateByIndex(index, name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");

        System.out.println("Enter project id:");
        final String id = TerminalUtil.nextLine();

        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        projectTaskService.removeProjectById(project.getId());
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");

        System.out.println("Enter project index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        projectTaskService.removeProjectById(project.getId());
        System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        int index = 1;
        final List<Project> projects = projectService.findAll();
        for (final Project project: projects) {
            if (project == null) continue;
            System.out.println(index++ + ". " + project.getName() + ": " + project.getDescription());
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");

        System.out.println("Enter project id:");
        final String id = TerminalUtil.nextLine();

        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();

        final Status status = Status.toStatus(statusValue);
        final Project project = projectService.changeProjectStatusById(id, status);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");

        System.out.println("Enter project index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();

        final Status status = Status.toStatus(statusValue);
        final Project project = projectService.changeProjectStatusByIndex(index, status);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT BY ID]");

        System.out.println("Enter project id:");
        final String id = TerminalUtil.nextLine();

        final Project project = projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT BY INDEX]");

        System.out.println("Enter project index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        final Project project = projectService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeProjectById() {
        System.out.println("[COMPLETE PROJECT BY ID]");

        System.out.println("Enter project id:");
        final String id = TerminalUtil.nextLine();

        final Project project = projectService.changeProjectStatusById(id, Status.COMPLETED);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");

        System.out.println("Enter project index:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        final Project project = projectService.changeProjectStatusByIndex(index, Status.COMPLETED);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
